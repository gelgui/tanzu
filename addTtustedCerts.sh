#!/bin/bash

get_cert(){
  echo "`echo | openssl s_client -connect $1:443 --showcerts 2>/dev/null | openssl x509|base64 -w 0`"

}

[ $# -eq 0 ] && { echo; echo;echo "Usage: $0 [domains]";echo;echo "For example: $0 example1.com example2.com > TanzuServiceConfiguration.yaml" 
                  echo "             Then apply it to the Supervisor Cluster";
                  echo "             kubectl apply -f ./TanzuServiceConfiguration.yaml";
                  echo; 
                  echo;
                  exit 1; }



cat <<EOF
apiVersion: run.tanzu.vmware.com/v1alpha1
kind: TkgServiceConfiguration
metadata:
  name: tkg-service-configuration

spec:
  defaultCNI: antrea
  trust:
    additionalTrustedCAs:
EOF
for domain in "$@"
do
  echo "      - name: $domain"
  echo -n "        data: "
  echo "`get_cert $domain`"
done




